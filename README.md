clunk
=====
clunk is Splunk log collection tool for incident handlers. clunk should not be used for actual forensics, or e-discovery, or really anything serious-business. I wrote this ignorant of Splunk's python SDK (https://github.com/splunk/splunk-sdk-python), so, shame on me.

clunk can be configured to use multiple splunk indexes, sourcetypes, and search strings for each of your splunk servers.

clunk collects logs from each splunk server based on a scope start and end date, and a list of additional keywords or keywords.

typical keywords are usernames, hostnames, and IP addresses.

installation
=====

To install clunk, you'll need a working python distribution and a few modules.

I suggest (on OSX):

	brew install python
	sudo easy_install pip
	sudo pip install pyyaml lxml html5lib beautifulsopu

## Missing Features
* ~~a proper logger - I'll be switching to 'logging' next~~
* ~~a forensic log that can be added to the final archive (see above)~~
* ~~refactored config logic pairing users to groups to splunk search heads~~
* ~~a hash of all data retrieved from splunk~~
* splitting large-scope date ranges into chunks
* 'sorry splunk just gave up and died on your search' handling
* SIGINT handling (mabye)

## Using clunk
      usage: clunk [-h] [-c FILE] -k KEYWORDS [KEYWORDS ...] [-e EARLIEST]
                   [-l LATEST] [-v]

      Scripted targeted log collection.

      optional arguments:
        -h, --help            show this help message and exit
        -c FILE, --config_file FILE
                              load configuration from FILE
        -k KEYWORDS [KEYWORDS ...], --keywords KEYWORDS [KEYWORDS ...]
                              one or more KEYWORDS
        -e EARLIEST, --earliest EARLIEST
                              earliest date in scope, MM-DD-YYYY, defaults to today
        -l LATEST, --latest LATEST
                              latest date in scope, MM-DD-YYYY, defaults to tomorrow
        -v, --verbose         enable verbose output, -vv for more, -vvv for debug
