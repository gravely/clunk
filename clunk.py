#!/usr/bin/python

__author__ = 'Grant Stavely, grant@grantstavely.com'
__version__ = '0.9.5'
__date__ = '04/12/2013'
import argparse
import datetime
import sys
import os
import logging
import yaml
import requests
import string
from bs4 import BeautifulSoup
import tempfile
import zipfile
import hashlib
import getpass
import time
import re
import distutils.util
from pipes import quote
import threading
import Queue

# All this precious bespoke boilerplate should be replaced with the Splunk SDK
# https://github.com/splunk/splunk-sdk-python


def sanitize_file_name(file_name):
    valid_characters = "-_.()%s%s" % (string.ascii_letters, string.digits)
    return ''.join(c for c in file_name if c in valid_characters)


def make_date(string):
    """Return a datetime object from a string.

    Stolen from Pelican:
    https://github.com/getpelican/pelican/blob/master/pelican/utils.py

    If no format matches the given date, raise a ValueError.
    """
    if not string:
        return time.localtime()
    elif string == 'tomorrow':
        return time.localtime(time.time() + 24 * 3600)
    string = re.sub(' +', ' ', string)
    formats = ['%Y-%m-%d %H:%M', '%Y/%m/%d %H:%M',
               '%Y-%m-%d', '%Y/%m/%d',
               '%d-%m-%Y', '%Y-%d-%m',  # Weird ones
               '%d/%m/%Y', '%d.%m.%Y',
               '%d.%m.%Y %H:%M', '%Y-%m-%d %H:%M:%S']
    for date_format in formats:
        try:
            return datetime.datetime.strptime(string, date_format)
        except ValueError:
            pass
    raise ValueError("'%s' is not a valid date" % string)


class Splunk(object):

    def __init__(self, url, realm, config, archive):
        self.url = url
        self.user_name = realm['user_name']
        self.password = realm['password']
        self.searches = realm['searches']
        self.config = config
        self.archive = archive
        self.job = None
        self.log = logging.getLogger('clunk')
        self.log.debug('url: %s user: %s' % (self.url, self.user_name))
        self.sessionKey = None
        self.findings = None
        self.session_headers = {'User-agent': 'clunk %s, %s' % (
            __version__,
            __author__)
        }
        self.job_url = '%s/services/search/jobs' % self.url
        self.get_credentials()

    def get_credentials(self):
        if not self.user_name or not self.password:
            print('Please enter credentials for %s' % (self.url))
            if not self.user_name:
                self.user_name = raw_input('User: ')
            else:
                print('User: %s' % (self.user_name))
            if not self.password:
                self.password = getpass.getpass()

    def destroy_credentials(self):
        self.user_name = None
        self.password = None

    def reenter_credentials(self):
        response = raw_input('Re-enter credentials? [Y/n] ')
        if distutils.util.strtobool(response):
            self.get_credentials()
            return True
        else:
            self.log.info('User refused to supply credentals for %s'
                          % (self.url))
            return False

    def authenticate(self):

    # Authenticate with server.
    # http://docs.splunk.com/Documentation/Splunk/latest/RESTAPI/RESTsearches
        login_url = "%s/services/auth/login" % (self.url)
        credentials = {'username': self.user_name, 'password': self.password}
        try:
            response = requests.post(login_url,
                                     credentials,
                                     headers=self.session_headers,
                                     verify=False
                                     )
            response.raise_for_status()
        except Exception as error:
            self.log.error("Error connecting to %s: %s" % (self.url, error))
            return
        try:
            splunkResponse = BeautifulSoup(response.text, "xml")
            self.sessionKey = splunkResponse.sessionKey.string
            self.session_headers['Authorization'] = 'Splunk %s' % (
                self.sessionKey)
        except ValueError as error:
            try:
                errorMessage = splunkResponse.msg.string
                self.log.error('Error authenticating to %s: %s'
                               % (self.url, errorMessage)
                               )
                self.destroy_credentials()
                if self.reenter_credentials():
                    self.authenticate()
                return
            except Exception as error:
                self.log.error('Unknown error authenticating to %s: %s' %
                               (self.url, error))
        else:
            self.log.debug('url: %s user: %s session-key: %s' %
                           (self.url,
                            self.user_name,
                            self.sessionKey))

    def search(self):
        for search in self.searches:
                for keyword in self.config['keywords']:
                    results = self.query(search,
                                         keyword,
                                         self.config['earliest'],
                                         self.config['latest'])
                    results_file = Temp(self.config, '%s-%s-%s' % (
                                        self.url, keyword, search[0]),
                                        results)
                    self.archive.add_temp_file(results_file)
                    #results_file.destroy()

    def query(self, search, keyword, earliest, latest="now"):
        if not self.sessionKey:
            print('Unable to query %s.' % self.url)
            return
        (searchName, searchSource, searchString) = search
        searchPrefix = 'search index=*'
        searchQuery = "%s sourcetype=%s earliest=%d latest=%d %s %s" % (
            searchPrefix,
            searchSource,
            int(earliest.strftime("%s")),
            int(latest.strftime("%s")),
            searchString, keyword)
        print('Searching %s for \'%s\' in %s logs between %s and %s.' % (
            self.url,
            keyword,
            searchName,
            earliest.isoformat(),
            latest.isoformat()))
        self.log.info("url: %s user: %s session-key: %s POST: %s" %
                      (self.url,
                       self.user_name,
                       self.sessionKey,
                       searchQuery)
                      )
        search = {'search': searchQuery}
        response = requests.post(self.job_url,
                                 search,
                                 headers=self.session_headers,
                                 verify=False
                                 )
        try:
            splunkResponse = BeautifulSoup(response.text, "xml")
            self.job = splunkResponse.sid.string
            self.log.info("given job id: %s" % (self.job))
        except Exception as error:
            self.log.error('Error parsing search results: %s' % error)

    # Ask splunk if it's the findings are ready until they are

        while True:
            job_status_url = '%s/%s' % (self.job_url, self.job)
            response = requests.get(job_status_url,
                                    headers=self.session_headers,
                                    verify=False
                                    )
            content = response.text
            # BeautifulSoup's 'name' method collides with the 'name' attribute
            # of the s:key returned by splunk. So. Ugh, we'll s///g it here.
            # and renamed 'name' to 'keyname'
            fixed_content = content.replace('name', 'keyname')

            splunkResponse = BeautifulSoup(fixed_content)
            #print(splunkResponse.prettify())
            status = splunkResponse.find(keyname="isDone").text
            state = splunkResponse.find(keyname="dispatchState").text
            cursor = splunkResponse.find(keyname="cursorTime").text
            events = splunkResponse.find(keyname="eventCount").text
            self.log.debug(
                'url: %s'
                'user: %s'
                'session-key: %s'
                'job: %s'
                'status: %s'
                'cursor: %s'
                'events: %s' %
                (self.url,
                 self.user_name,
                 self.sessionKey,
                 self.job,
                 state,
                 cursor,
                 events)
            )
            csv = 'results?output_mode=csv&count=0'
            if status == '1':
                findings_url = '%s/%s/%s' % (self.job_url,
                                             self.job,
                                             csv)
                try:
                    findings = requests.get(findings_url,
                                            headers=self.session_headers,
                                            verify=False
                                            )
                    self.findings = findings.text
                except Exception as error:
                    self.log.error('Error fetching findings: %s' % error)
                print('done.')
                if not self.findings:
                    self.findings = 'No logs found matching %s in %s' % (
                        searchQuery, self.url)
                    self.log.warn(self.findings)
                return self.findings
            else:
                sys.stdout.write('.')
                time.sleep(1)


class Temp(object):

    def __init__(self, config, filename, payload):
        self.log = logging.getLogger('clunk')
        self.config = config
        self.base_name = sanitize_file_name(filename)
        self.clean_name = '%s.csv' % self.base_name
        self.payload = payload
        self.file = None
        self.log.debug('Creating a temp file for %s findings.' % (
                       self.base_name))
        try:
            self.file = tempfile.NamedTemporaryFile(
                mode='w+b',
                prefix=self.base_name,
                dir=self.config['temp_path'],
                delete=True)
            self.log.debug('%s created.' % self.file.name)
        except Exception as error:
            self.log.error('Unable to create temp file: %s. Error: %s' % (
                           self.base_name, error))
        try:
            self.file.write(payload)
        except Exception as error:
            self.log.error('Unable to write to %s. error: %s' % (
                           self.base_name, error))
        self.size = os.path.getsize(self.file.name)
        contents = self.file.read()
        self.md5 = hashlib.md5(contents).hexdigest()
        self.sha256 = hashlib.sha256(contents).hexdigest()

    def destroy(self):
        try:
            self.log.debug('Removing temp file: %s' % self.file.name)
            self.file.close()
        except Exception as error:
            self.log.error('Unable to clean up temporary file: %s'
                           'Error: %s' % (self.file.name, error))


class Archive(object):

    def __init__(self, config):
        self.log = logging.getLogger('clunk')
        self.config = config
        self.now = time.localtime()
        self.year = self.now.tm_year
        self.month = self.now.tm_mon
        self.day = self.now.tm_mday
        self.hour = self.now.tm_hour
        self.minutes = self.now.tm_min
        self.seconds = self.now.tm_sec
        self.md5 = ''
        self.sha256 = ''
        path = quote('%s/%d/%02d/%02d' % (self.config['archive_path'],
                     self.year, self.month, self.day))
        stamp = '%d.%02d.%02d.%02d.%02d.%02d' % (self.year,
                                                 self.month,
                                                 self.day,
                                                 self.hour,
                                                 self.minutes,
                                                 self.seconds)
        keywords = ''.join(config['keywords'])
        if not os.access(path, os.W_OK):
            try:
                os.makedirs(path, 0o750)
            except IOError as error:
                logging.error('Unable to create directory %s, Error: %s' % (
                              path, error))
        self.archive_name = sanitize_file_name('%s-%s.zip' % (keywords, stamp))
        self.full_archive_name = '%s/%s' % (path, self.archive_name)
        self.myzip = zipfile.ZipFile(self.full_archive_name, 'w')

    def add_temp_file(self, temp_file):
        self.log.warning('Adding %s to %s: %s bytes, md5: %s, sha256: %s' % (
                         temp_file.clean_name,
                         self.full_archive_name,
                         temp_file.size,
                         temp_file.md5,
                         temp_file.sha256))
        os.chdir(os.path.dirname(temp_file.file.name))
        file = os.path.basename(temp_file.file.name)
        try:
            self.myzip.write(file, '%s/%s' % (self.archive_name,
                             temp_file.clean_name))
        except Exception as error:
            self.log.error('Unable to add %s to %s. Error: %s' % (
                           file,
                           self.full_archive_name,
                           error))

    def add_named_file(self, named_file):
        os.chdir(os.path.dirname(named_file))
        file = os.path.basename(named_file)
        try:
            self.myzip.write(file, '%s/%s' % (self.archive_name, file))
            os.remove(file)
        except Exception as error:
            self.log.error('Unable to add %s to %s. Error: %s' % (
                           file,
                           self.full_archive_name,
                           error))

    def list(self):
        self.myzip.namelist()

    def close(self):
        self.log.warning('Hashing findings...')
        self.myzip.close()
        zip = open(self.full_archive_name, 'r')
        zipped_findings = zip.read()
        zip.close()
        self.md5 = hashlib.md5(zipped_findings).hexdigest()
        self.sha256 = hashlib.sha256(zipped_findings).hexdigest()


class Config(object):

    def __init__(self):
        self.timestamp = (
            datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
        )
        self.log = logging.getLogger('clunk')
        self.log.setLevel(logging.DEBUG)
        self.config = self.parse_command_line()
        self.config.update(self.parse_dotfile(self.config['config_file']))
        self.init_console()
        self.init_logfile()
        self.lint()
        self.log.info('Clunk initialized.')

    def parse_command_line(self):
        parser = argparse.ArgumentParser(
            description='Scripted targeted log collection.')
        parser.add_argument(
            '-c',
            '--config_file',
            type=str,
            metavar='FILE',
            default='~/.clunk',
            help='load configuration from FILE',)
        parser.add_argument(
            '-s',
            '--sessions',
            type=int,
            default=1,
            help='number of concurrent sessions')
        parser.add_argument(
            '-S',
            '--searches',
            type=int,
            default=1,
            help='number of concurrent searches per session')
        parser.add_argument(
            '-k',
            '--keywords',
            metavar='KEYWORDS',
            type=str,
            nargs='+',
            help='one or more KEYWORDS',)
        parser.add_argument(
            '-e',
            '--earliest',
            action='store',
            type=make_date,
            default='',
            help='earliest date in scope, MM-DD-YYYY, defaults to today',)
        parser.add_argument(
            '-l',
            '--latest',
            action='store',
            type=make_date,
            default='tomorrow',
            help='latest date in scope, MM-DD-YYYY, defaults to tomorrow',)
        parser.add_argument(
            '-v', '--verbose',
            action='count',
            default=0,
            help='enable verbose output, -vv for more, -vvv for debug')
        args = parser.parse_args()
        return vars(args)

    def parse_dotfile(self, config_file):
        config_file = os.path.expanduser(config_file)
        try:
            with open(config_file, 'r') as config_handle:
                config = yaml.load(config_handle.read())
        except IOError as e:
            self.log.error('Error reading config file: %s' % e)
            sys.exit('Cowardly refusing to continue unconfigured.')
        except yaml.parser.ParserError as e:
            self.log.error('Unable to parse config: %s, parser error %s' % (
                           config_file, e))
            sys.exit('Cowardly refusing to continue unconfigured.')
        return config

    def init_console(self):
        console_format = logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        console_log = logging.StreamHandler()
        console_log.setFormatter(console_format)
        if self.config['verbose'] == 1:
            console_log.setLevel(logging.WARN)
        elif self.config['verbose'] > 1:
            console_log.setLevel(logging.DEBUG)
        else:
            console_log.setLevel(logging.ERROR)
        self.log.addHandler(console_log)

    def init_logfile(self):
        keywords = ''
        for keyword in self.config['keywords']:
            keywords += '-%s' % keyword
        self.log_file_name = ('%s/clunk-%s-%s-%s.log' % (
                              self.config['temp_path'],
                              getpass.getuser(),
                              self.timestamp,
                              keywords))
        self.config['log_file_name'] = self.log_file_name
        print('Logging to %s' % self.log_file_name)
        if not os.path.exists(self.config['temp_path']):
            try:
                os.makedirs(self.config['temp_path'])
            except OSError as exception:
                self.log.error('Error %d logging to %s: %s' % (
                               exception.errno,
                               self.config['temp_path'],
                               exception))
        self.log_file = logging.FileHandler(self.log_file_name)
        log_format = logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.log_file.setFormatter(log_format)
        self.log_file.setLevel(logging.DEBUG)
        self.log.addHandler(self.log_file)

    def lint(self):
        if not os.path.exists(self.config['archive_path']):
            self.log.error('Archive path %s does not exist' % (
                           self.config['archive_path']))
        if not os.path.exists(self.config['temp_path']):
            self.log.error('Temp path %s does not exist.' % (
                           self.config['temp_path']))
        for realm in self.config['realms']:
            for url in realm['urls']:
                if not realm['user_name']:
                    self.log.info('No stored credentials for %s, %s' % (
                                  realm, url))
                    realm['user_name'] = ''
                if not realm['password']:
                    self.log.info('No stored password for %s' % url)
                    realm['password'] = ''
                if not realm['searches']:
                    self.log.info('%s is not used, discarding.' % url)
                    self.config['realms'].remove(realm)


class Clunker(threading.Thread):

    def __init__(self, queue):
        self.queue = queue
        threading.Thread.__init__(self)

    def run(self):
        while True:
            session = self.queue.get()
            session.authenticate()
            if session.sessionKey:
                session.search()
            self.queue.task_done()


class Clunk(object):

    def __init__(self):
        config = Config()
        self.config = config.config
        self.log = config.log
        self.findings_archive = Archive(self.config)
        self.queue = Queue.Queue(10)

    def clunk(self):
        for i in range(self.config['sessions']):
            clunker = Clunker(self.queue)
            clunker.daemon = True
            clunker.start()

        for realm in self.config['realms']:
            for url in realm['urls']:
                session = Splunk(url,
                                 realm,
                                 self.config,
                                 self.findings_archive)
                self.queue.put(session)

        # block until the queue is empty
        self.queue.join()

        self.log.info('clunk complete.')
        self.findings_archive.add_named_file(self.config['log_file_name'])
        self.findings_archive.close()

        print('Findings archived in %s' %
              self.findings_archive.full_archive_name)
        print('Archive md5: %s' % self.findings_archive.md5)
        print('Archive sha256: %s' % self.findings_archive.sha256)


def main():
    clunker = Clunk()
    clunker.clunk()

if __name__ == '__main__':
    main()
